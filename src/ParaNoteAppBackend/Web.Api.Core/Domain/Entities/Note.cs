﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Web.Api.Core.Shared;

namespace Web.Api.Core.Domain.Entities
{
    public class Note : BaseEntity
    {
        [Required]
        [MaxLength(256)]
        public string Title { get; set; }


        [Required]
        [MaxLength(4096)]
        public string Description { get; set; }

        [Required]
        public DateTime DueDate { get; set; }

        [Required]
        public int Importance { get; set; }

        [Required]
        public bool Done { get; set; }

        public DateTime? DoneDate { get; set; }

        internal Note() {}

        internal Note(string title, string description, DateTime dueDate, int importance, bool done = false, DateTime? doneDate = null)
            : this()
        {
            this.Title = title;
            this.Description = description;
            this.DueDate = dueDate;
            this.Importance = importance;
            this.Done = done;
            this.DoneDate = doneDate;
        }

        public override bool Equals(object obj)
        {
            return (obj is Note) && ((Note)obj).Id == this.Id;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

    }
}
