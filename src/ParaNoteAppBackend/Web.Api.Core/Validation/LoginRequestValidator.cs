﻿using FluentValidation;
using Web.Api.Core.Dto.UseCaseRequests;

namespace Web.Api.DtoModels.Validation
{
    public class LoginRequestValidator : AbstractValidator<LoginRequest>
    {
        public LoginRequestValidator()
        {
            RuleFor(x => x.UserName).NotEmpty();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
