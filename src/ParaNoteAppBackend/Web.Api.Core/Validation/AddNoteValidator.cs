﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using Web.Api.Core.Dto
    ;

namespace Web.Api.Core.Validation
{
    public class AddNoteValidator : AbstractValidator<DtoAddNoteRequest>
    {
        public AddNoteValidator()
        {
            RuleFor(x => x.Title).NotEqual("");
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.Title).NotNull();
            RuleFor(x => x.Importance).GreaterThanOrEqualTo(1).LessThanOrEqualTo(5);
        }
    }
}
