﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public sealed class AssignUserToRoleUseCase : IAssignUserToRoleUseCase
    {

        private readonly IUserRepository userRepository;

        public AssignUserToRoleUseCase(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public async Task<bool> Handle(AssignRoleToUserRequest message, IOutputPort<AssignRoleToUserResponse> outputPort)
        {
            var response = await this.userRepository.AssignRole(message.UserName, message.NameRole);
            outputPort.Handle(response.Success ? new AssignRoleToUserResponse(true)
                : new AssignRoleToUserResponse(response.Errors));
            return response.Success;

        }
    }
}
