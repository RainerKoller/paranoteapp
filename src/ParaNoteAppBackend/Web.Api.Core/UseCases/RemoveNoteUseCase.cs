﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public class RemoveNoteUseCase : IRemoveNoteUseCase
    {

        private readonly INoteRepository noteRepository;
        private readonly IUserRepository userRepository;

        public RemoveNoteUseCase(INoteRepository noteRepository, IUserRepository userRepository)
        {
            this.noteRepository = noteRepository;
            this.userRepository = userRepository;
        }

        public async Task<bool> Handle(RemoveNoteRequest message, IOutputPort<ManipulateNoteResponse> outputPort)
        {
            var errs = new List<Error>();
            const string FAILURE_MSG = "Failure at remove note";

            var curUser = await this.userRepository.FindByName(message.UserName);

            if (null == curUser)
            {
                errs.Add(new Error("-1", "Could not resolve user"));
                outputPort.Handle(new ManipulateNoteResponse(errs, false, FAILURE_MSG));
                return false;
            }

            var note = await this.noteRepository.GetById(message.Id);

            if (null == note)
            {
                var error = new Error("-2", "Note not found");
                var errors = new List<Error>();
                errors.Add(error);
                outputPort.Handle(new ManipulateNoteResponse(errors, false, FAILURE_MSG));
                return false;
            }

            if (!curUser.Notes.Contains(note))
            {
                errs.Add(new Error("-3", $"User {message.UserName} is not the owner of note with id: {message.Id}"));
                outputPort.Handle(new ManipulateNoteResponse(errs, false, FAILURE_MSG));
                return false;
            }

            await this.noteRepository.Delete(note);

            outputPort.Handle(new ManipulateNoteResponse(message.Id, true));
            return true;
        }
    }
}
