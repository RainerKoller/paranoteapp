﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public sealed class UpdateNoteUseCase : IUpdateNoteUseCase
    {

        private readonly INoteRepository noteRepository;
        private readonly IUserRepository userRepository;

        public UpdateNoteUseCase(INoteRepository noteRepository, IUserRepository userRepository)
        {
            this.noteRepository = noteRepository;
            this.userRepository = userRepository;
        }

        public async Task<bool> Handle(UpdateNoteRequest message, IOutputPort<ManipulateNoteResponse> outputPort)
        {
            var errs = new List<Error>();
            const string FAILURE_MSG = "Failure at updating note";

            var curUser = await this.userRepository.FindByName(message.UserName);

            if (curUser is null)
            {
                errs.Add(new Error("-1", "Could not resolve user"));
                outputPort.Handle(new ManipulateNoteResponse(errs, false, FAILURE_MSG));
                return false;
            }

            var note = await this.noteRepository.GetById(message.Id);

            if (note is null)
            {
                errs.Add(new Error("-2", "Could not resolve note"));
                outputPort.Handle(new ManipulateNoteResponse(errs, false, FAILURE_MSG));
                return false;
            }

            if (!curUser.Notes.Contains(note)) 
            {
                errs.Add(new Error("-3", $"User {message.UserName} is not the owner of note with id: {message.Id}"));
                outputPort.Handle(new ManipulateNoteResponse(errs, false, FAILURE_MSG));
                return false;
            }

            var answer = await this.noteRepository
                .Update(message.Id, message.Title, message.Description, 
                message.DueDate, message.Importance, message.Done);

            if (answer is null)
            {
                errs.Add(new Error("-4", $"Could not update note with id {message.Id}"));
                outputPort.Handle(new ManipulateNoteResponse(errs, false, FAILURE_MSG));
                return false;
            }
            outputPort.Handle(new ManipulateNoteResponse(answer.Id, true));
            return true;
        }
    }
}
