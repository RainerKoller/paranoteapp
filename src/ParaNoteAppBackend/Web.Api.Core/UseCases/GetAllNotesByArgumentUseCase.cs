﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public sealed class GetAllNotesByArgumentUseCase : IGetAllNotesByArgumentUseCase
    {
        private readonly INoteRepository noteRepository;
        private readonly IUserRepository userRepository;

        public GetAllNotesByArgumentUseCase(INoteRepository noteRepository, IUserRepository userRepository)
        {
            this.noteRepository = noteRepository;
            this.userRepository = userRepository;
        }

        public async Task<bool> Handle(GetAllNotesByArgumentRequest message, IOutputPort<GetAllNotesByArgumentResponse> outputPort)
        {
            var errs = new List<Error>();
            List<Note> ret;

            var curUser = await this.userRepository.FindByName(message.UserName);

            if (null != message.Strategy)
            {
                ret = await this.noteRepository.GetByStrategy(message.Strategy, curUser);

                if (null != ret)
                {
                    outputPort.Handle(new GetAllNotesByArgumentResponse(ret, true));
                    return true;
                }
                else
                {
                    errs.Add(new Error("1", "Failed to fetch test runs"));
                }
                outputPort.Handle(new GetAllNotesByArgumentResponse(errs, false, "No test runs found"));
                return false;
            }
            else 
            {
                ret = await this.noteRepository.GetByUser(curUser);

                if (null != ret)
                {
                    outputPort.Handle(new GetAllNotesByArgumentResponse(ret, true));
                    return true;
                }
                else
                {
                    errs.Add(new Error("1", "Failed to fetch test runs"));
                }
                outputPort.Handle(new GetAllNotesByArgumentResponse(errs, false, "No test runs found"));
                return false;
            }
        }
    }
}
