﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.UseCases;

namespace Web.Api.Core.UseCases
{
    public sealed class AddNoteUseCase : IAddNoteUseCase
    {

        private readonly INoteRepository noteRepository;
        private readonly IUserRepository userRepository;

        public AddNoteUseCase(INoteRepository noteRepository, IUserRepository userRepository)
        {
            this.noteRepository = noteRepository;
            this.userRepository = userRepository;
        }

        public async Task<bool> Handle(AddNoteRequest message, IOutputPort<ManipulateNoteResponse> outputPort)
        {
            var curUser = await this.userRepository.FindByName(message.UserName);

            var errs = new List<Error>();

            if (null != curUser)
            {
                var answer = await this.noteRepository
                    .Add(message.Title, message.Description, message.DueDate, message.Importance, curUser.Id);

                if (null == answer)
                {
                    errs.Add(new Error("-1", "Could not add a note"));
                    outputPort.Handle(new ManipulateNoteResponse(errs, false, "Failure at creating note"));
                    return false;
                }

                var note = await this.noteRepository.GetById(answer.Id);

                curUser.Notes.Add(note);

                await this.userRepository.Update(curUser);

                outputPort.Handle(new ManipulateNoteResponse(answer.Id, true));
                return true;
            }
            errs.Add(new Error("-2", "Could not resolve user"));
            outputPort.Handle(new ManipulateNoteResponse(errs, false, "Failure at creating note"));
            return false;
        }
    }
}
