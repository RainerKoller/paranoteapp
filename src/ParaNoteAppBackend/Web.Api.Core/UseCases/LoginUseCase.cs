﻿using System.Threading.Tasks;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.Services;
using Web.Api.Core.Interfaces.UseCases;


namespace Web.Api.Core.UseCases
{
    public sealed class LoginUseCase : ILoginUseCase
    {
        private readonly IUserRepository userRepository;
        private readonly IJwtFactory jwtFactory;
        private readonly ITokenFactory tokenFactory;

        public LoginUseCase(IUserRepository userRepository, IJwtFactory jwtFactory, ITokenFactory tokenFactory)
        {
            this.userRepository = userRepository;
            this.jwtFactory = jwtFactory;
            this.tokenFactory = tokenFactory;
        }

        public async Task<bool> Handle(LoginRequest message, IOutputPort<LoginResponse> outputPort)
        {
            if (!string.IsNullOrEmpty(message.UserName) && !string.IsNullOrEmpty(message.Password))
            {
                // ensure we have a user with the given user name
                var user = await this.userRepository.FindByName(message.UserName);
                if (user != null)
                {
                    var usersRoles = await this.userRepository.GetUsersRoles(user);
                    if (null != usersRoles) 
                    {
                        // validate password
                        if (await this.userRepository.CheckPassword(user, message.Password))
                        {
                            // generate refresh token
                            var refreshToken = this.tokenFactory.GenerateToken();
                            user.AddRefreshToken(refreshToken, user.Id, message.RemoteIpAddress);
                            await this.userRepository.Update(user);

                            // generate access token
                            outputPort.Handle(new LoginResponse(
                                await this.jwtFactory.GenerateEncodedToken(user.IdentityId, user.UserName, usersRoles), refreshToken, true));
                            return true;
                        }
                    }
                }
            }
            outputPort.Handle(new LoginResponse(new[] { new Error("login_failure", "Invalid username or password.") }));
            return false;
        }
    }
}
