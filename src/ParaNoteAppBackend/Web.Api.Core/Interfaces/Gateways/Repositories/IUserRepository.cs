﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Dto.GatewayResponses.Repositories;
using Web.Api.Core.Dto.UseCaseResponses;

namespace Web.Api.Core.Interfaces.Gateways.Repositories
{
    public interface IUserRepository  : IRepository<User>
    {
        Task<CreateUserResponse> Create(string firstName, string lastName, string email, string userName, string password);
        Task<AssignRoleToUserResponse> AssignRole(string userName, string nameRole);
        Task<User> FindByName(string userName);
        Task<List<string>> GetUsersRoles(User user);
        Task<bool> CheckPassword(User user, string password);
    }
}
