﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces.Strategies;

namespace Web.Api.Core.Interfaces.Gateways.Repositories
{
    public interface INoteRepository : IRepository<Note>
    {
        Task<ManipulateNoteResponse> Add(string title, string description,
            DateTime duedate, int importance, long userId);
        Task<ManipulateNoteResponse> Update(long id, string title, string description,
            DateTime duedate, int importance, bool done);

        Task<List<Note>> GetByStrategy(IGetNotesStrategy strategy, User user);

        Task<List<Note>> GetByUser(User user);
    }
}
