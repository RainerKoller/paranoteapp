﻿
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Web.Api.Core.Dto;

namespace Web.Api.Core.Interfaces
{
    public abstract class UseCaseResponseMessage
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public IEnumerable<Error> Errors { get; set; }

        protected bool inErrorState = false;

        protected UseCaseResponseMessage(bool success = false, string message = null)
        {
            Success = success;
            Message = message;
        }
    }
}
