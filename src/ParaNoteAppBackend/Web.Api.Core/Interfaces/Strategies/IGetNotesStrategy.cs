﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;

namespace Web.Api.Core.Interfaces.Strategies
{
    public interface IGetNotesStrategy
    {
        Task<List<Note>> GetNotes(object dbcontext, User user);
    }
}
