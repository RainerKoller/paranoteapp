﻿using System;

namespace Web.Api.Core.Exceptions
{
    public class NoElementsFoundException : Exception
    {
        public new string Message { get; }
        public NoElementsFoundException(string msg)
        {
            this.Message = msg;
        }
    }
}
