﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Api.Core.Dto
{
    public class DtoAddNoteRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public int Importance { get; set; }

        public DtoAddNoteRequest() { } // needed for deserialization
        public DtoAddNoteRequest(string title, string description, DateTime duedate, int importance)
        {
            this.Title = title;
            this.Description = description;
            this.DueDate = duedate;
            this.Importance = importance;
        }
    }
}
