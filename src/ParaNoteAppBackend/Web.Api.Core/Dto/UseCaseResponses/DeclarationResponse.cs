﻿using System;
using System.Collections.Generic;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseResponses
{
    public class DeclarationResponse : UseCaseResponseMessage
    {
        public long Id { get; set; }
        public bool IsExisting { get; set; }

        public DeclarationResponse(IEnumerable<Error> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
            this.inErrorState = true;
        }

        public DeclarationResponse(long id, bool isExisting, bool success = false, string message = null) : base(success, message)
        {
            this.Id = id;
            this.IsExisting = isExisting;
        }

        public DeclarationResponse() { }

        public bool ShouldSerializeErrors()
        {
            return this.inErrorState == true;
        }
        public bool ShouldSerializeId()
        {
            return this.inErrorState == false;
        }
        public bool ShouldSerializeIsExisting()
        {
            return this.inErrorState == false;
        }
    }
}
