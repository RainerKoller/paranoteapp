﻿using System.Collections.Generic;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseResponses
{
    public class ManipulateNoteResponse : UseCaseResponseMessage
    {
        public long Id { get; set; }

        public ManipulateNoteResponse(IEnumerable<Error> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
            this.inErrorState = true;
        }

        public ManipulateNoteResponse(long id, bool success = false, string message = null) : base(success, message)
        {
            this.Id = id;
        }

        public ManipulateNoteResponse() { }

        public bool ShouldSerializeErrors()
        {
            return this.inErrorState == true;
        }
        public bool ShouldSerializeId()
        {
            return this.inErrorState == false;
        }
    }
}
