﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseResponses
{
    public class GetAllNotesByArgumentResponse : UseCaseResponseMessage
    {

        public List<DtoNote> Notes { get; set; }

        public int Count { get; set; }

        private IMapper iMapper;

        public GetAllNotesByArgumentResponse(IEnumerable<Error> errors, bool success = false, string message = null)
            : base(success, message)
        {
            this.Errors = errors;
            this.inErrorState = true;
        }

        public GetAllNotesByArgumentResponse() { }

        public GetAllNotesByArgumentResponse(List<Note> notes, bool success = false, string message = null) : base(success, message)
        {
            this.Notes = new List<DtoNote>();
            //var config = new MapperConfiguration(cfg =>
            //{
            //    cfg.CreateMap<Note, DtoNote>().ForMember(dest => dest.CreatedDate, 
            //        opt => opt.MapFrom(src => 
            //        new DateTime(
            //            src.Created.Date.Year, src.Created.Date.Month, src.Created.Date.Day, 
            //            src.Created.TimeOfDay.Hours, src.Created.TimeOfDay.Minutes, src.Created.TimeOfDay.Seconds)));
            //});

            //this.iMapper = config.CreateMapper();


            foreach (var n in notes) 
            {
                var d = new DtoNote();
                d.Id = n.Id;
                d.Title = n.Title;
                d.Description = n.Description;
                d.DueDate = n.DueDate;
                d.Importance = n.Importance;
                d.CreatedDate = n.Created;
                d.DoneDate = n.DoneDate;
                d.Done = n.Done;
                this.Notes.Add(d);
            }

            //try
            //{
            //    this.Notes = this.iMapper.Map<List<Note>, List<DtoNote>>(notes);
            //}
            //catch (Exception e) 
            //{
            //    this.inErrorState = true;
            //    this.Message = e.Message;
            //}
            this.Count = this.Notes.Count;
        }
    }
}
