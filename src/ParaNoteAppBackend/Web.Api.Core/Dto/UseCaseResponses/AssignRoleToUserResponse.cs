﻿using System.Collections.Generic;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseResponses
{
    public class AssignRoleToUserResponse : UseCaseResponseMessage
    {
        public AssignRoleToUserResponse(IEnumerable<Error> errors, bool success = false, string message = null) : base(success, message)
        {
            Errors = errors;
        }

        public AssignRoleToUserResponse(bool success = false, string message = null) : base(success, message)
        {
        }

        public AssignRoleToUserResponse() { }

    }
}
