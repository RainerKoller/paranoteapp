﻿using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Core.Interfaces.Strategies;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class GetAllNotesByArgumentRequest : IUseCaseRequest<GetAllNotesByArgumentResponse>
    {
        public IGetNotesStrategy Strategy { get;  }

        public string UserName { get; }

        public GetAllNotesByArgumentRequest(string userName)
        {
            this.Strategy = null;
            this.UserName = userName;
        }

        public GetAllNotesByArgumentRequest(IGetNotesStrategy strategy, string userName)
        {
            this.Strategy = strategy;
            this.UserName = userName;
        }
    }
}
