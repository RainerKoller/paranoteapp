﻿using System;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class AddNoteRequest : IUseCaseRequest<ManipulateNoteResponse>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public int Importance { get; set; }
        public string UserName { get; set; }

        public AddNoteRequest() {  } // needed for deserialization
        public AddNoteRequest(string title, string description, DateTime duedate, int importance, string userName)
        {
            this.Title = title;
            this.Description = description;
            this.DueDate = duedate;
            this.Importance = importance;
            this.UserName = userName;
        }
    }
}
