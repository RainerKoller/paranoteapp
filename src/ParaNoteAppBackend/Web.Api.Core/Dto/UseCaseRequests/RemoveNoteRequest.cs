﻿using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class RemoveNoteRequest : IUseCaseRequest<ManipulateNoteResponse>
    {
        public long Id { get; set; }
        public string UserName { get; set; }

        public RemoveNoteRequest(long id, string userName)
        {
            this.Id = id;
            this.UserName = userName;
        }
    }
}
