﻿using System;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class UpdateNoteRequest : IUseCaseRequest<ManipulateNoteResponse>
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public int Importance { get; set; }
        public bool Done { get; set; }
        public string UserName { get; set; }

        public UpdateNoteRequest() { } // needed for deserialization
        public UpdateNoteRequest(long id, string title, string description, DateTime duedate, int importance, bool done, string userName)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.DueDate = duedate;
            this.Importance = importance;
            this.Done = done;
            this.UserName = userName;
        }
    }
}
