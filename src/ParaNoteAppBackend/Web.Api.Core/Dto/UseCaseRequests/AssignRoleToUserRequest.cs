﻿using System;
using System.Collections.Generic;
using System.Text;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Core.Dto.UseCaseRequests
{
    public class AssignRoleToUserRequest : IUseCaseRequest<AssignRoleToUserResponse>
    {
        public string UserName { get; set; }
        public string NameRole { get; set; }

        public AssignRoleToUserRequest() { } // needed for deserialization

        public AssignRoleToUserRequest(string userName, string nameRole)
        {
            this.UserName = userName;
            this.NameRole = nameRole;
        }
    }
}
