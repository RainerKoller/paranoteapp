﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Api.Core.Dto
{
    public class DtoUpdateNoteRequest
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        public int Importance { get; set; }
        public bool Done { get; set; }
        public DtoUpdateNoteRequest() { } // needed for deserialization
        public DtoUpdateNoteRequest(long id, string title, string description, DateTime duedate, int importance, bool done)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.DueDate = duedate;
            this.Importance = importance;
            this.Done = done;
        }
    }
}
