﻿using System;

namespace Web.Api.Core.Dto
{
    public class DtoNote
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime DueDate { get; set; }

        public int Importance { get; set; }

        public bool Done { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime? DoneDate { get; set; }

        public DtoNote() { } // needed by the serializer

        public DtoNote(long id, string title, string description, DateTime duedate, 
            int importance, DateTime createdDate, bool done, DateTime? doneDate)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.DueDate = duedate;
            this.Importance = importance;
            this.CreatedDate = createdDate;
            this.Done = done;
            this.DoneDate = doneDate;
        }
    }
}
