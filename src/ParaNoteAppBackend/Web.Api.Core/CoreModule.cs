﻿using Autofac;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Core.UseCases;

namespace Web.Api.Core
{
    public class CoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Administrative UseCases
            builder.RegisterType<ExchangeRefreshTokenUseCase>().As<IExchangeRefreshTokenUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<RegisterUserUseCase>().As<IRegisterUserUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<AddNoteUseCase>().As<IAddNoteUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<UpdateNoteUseCase>().As<IUpdateNoteUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<RemoveNoteUseCase>().As<IRemoveNoteUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<GetAllNotesByArgumentUseCase>().As<IGetAllNotesByArgumentUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<AssignUserToRoleUseCase>().As<IAssignUserToRoleUseCase>().InstancePerLifetimeScope();
            builder.RegisterType<LoginUseCase>().As<ILoginUseCase>().InstancePerLifetimeScope();
        }
    }
}
