﻿using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Presenters;

namespace Web.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IRegisterUserUseCase registerUserUseCase;
        private readonly RegisterUserPresenter registerUserPresenter;

        private readonly IAssignUserToRoleUseCase assignUserToRoleUseCase;
        private readonly AssignUserToRolePresenter assignUserToRolePresenter;

        public AccountsController([FromServices] IRegisterUserUseCase registerUserUseCase,
            [FromServices] IAssignUserToRoleUseCase assignUserToRoleUseCase)
        {
            this.registerUserUseCase = registerUserUseCase;
            this.registerUserPresenter = new RegisterUserPresenter();

            this.assignUserToRoleUseCase = assignUserToRoleUseCase;
            this.assignUserToRolePresenter = new AssignUserToRolePresenter();

        }

        //POST api/v1/Accounts/Register
        [HttpPost("Register")]
        public async Task<ActionResult> Register([FromBody] RegisterUserRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await this.registerUserUseCase.Handle(new RegisterUserRequest(request.FirstName, request.LastName, request.Email, request.UserName, request.Password), 
                this.registerUserPresenter);
            return this.registerUserPresenter.ContentResult;
        }

        //POST api/v1/Accounts/AssignRole
        [HttpPost("AssignRole")]
        public async Task<ActionResult> AssignRole([FromBody] AssignRoleToUserRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await this.assignUserToRoleUseCase.Handle(new AssignRoleToUserRequest(request.UserName, request.NameRole),
                this.assignUserToRolePresenter);
            return this.assignUserToRolePresenter.ContentResult;
        }
    }
}
