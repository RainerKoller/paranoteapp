﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Interfaces.Strategies;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Helpers;
using Web.Api.Presenters;

namespace Web.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly IAddNoteUseCase addNoteUseCase;
        private readonly AddNotePresenter addNotePresenter;

        private readonly IUpdateNoteUseCase updateNoteUseCase;
        private readonly UpdateNotePresenter updateNotePresenter;

        private readonly IGetAllNotesByArgumentUseCase getAllNotesByArgumentUseCase;
        private readonly GetAllNotesByArgumentPresenter getAllNotesByArgumentPresenter;

        private readonly IRemoveNoteUseCase removeNoteUseCase;
        private readonly RemoveNotePresenter removeNotePresenter;

        private StrategyCreatorGetNotes queryCreator;


        public NotesController([FromServices] IAddNoteUseCase addNoteUseCase, 
            [FromServices] IUpdateNoteUseCase updateNoteUseCase,
            [FromServices] IGetAllNotesByArgumentUseCase getAllNotesByArgumentUseCase,
            [FromServices] IRemoveNoteUseCase removeNoteUseCase)
        {
            this.addNoteUseCase = addNoteUseCase;
            this.addNotePresenter = new AddNotePresenter();

            this.updateNoteUseCase = updateNoteUseCase;
            this.updateNotePresenter = new UpdateNotePresenter();

            this.getAllNotesByArgumentUseCase = getAllNotesByArgumentUseCase;
            this.getAllNotesByArgumentPresenter = new GetAllNotesByArgumentPresenter();

            this.removeNoteUseCase = removeNoteUseCase;
            this.removeNotePresenter = new RemoveNotePresenter();

            this.queryCreator = new StrategyCreatorGetNotes();
        }

        // POST api/v1/notes
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Authorize(Roles = "RoleUser")]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] DtoAddNoteRequest request)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            var currentUser = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await this.addNoteUseCase.Handle(
                new AddNoteRequest(request.Title, request.Description, request.DueDate, 
                request.Importance, currentUser), this.addNotePresenter);
            return this.addNotePresenter.ContentResult;
        }

        // PUT api/v1/notes
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Authorize(Roles = "RoleUser")]
        [HttpPut]
        public async Task<ActionResult> Put([FromBody] DtoUpdateNoteRequest request)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            var currentUser = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await this.updateNoteUseCase.Handle(
                new UpdateNoteRequest(request.Id, request.Title, request.Description, 
                request.DueDate, request.Importance, request.Done, currentUser), 
                this.updateNotePresenter);
            return this.updateNotePresenter.ContentResult;
        }

        // GET api/v1/notes
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Authorize(Roles = "RoleUser")]
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            var currentUser = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await this.getAllNotesByArgumentUseCase.Handle(
                new GetAllNotesByArgumentRequest(currentUser),
                this.getAllNotesByArgumentPresenter);
            return this.getAllNotesByArgumentPresenter.ContentResult;
        }

        // GET api/v1/notes/{key}/{value}
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Authorize(Roles = "RoleUser")]
        [HttpGet]
        [Route("{Key}/{Value}")] // i.e. /done/false
        public async Task<ActionResult> Get([FromRoute] string Key, [FromRoute] string Value)
        {
            IGetNotesStrategy strategy = this.queryCreator.CreateNotesQueryStrategy(Key, Value);
            if (strategy is null)
            {
                this.getAllNotesByArgumentPresenter.Handle(
                    new Core.Dto.UseCaseResponses.GetAllNotesByArgumentResponse(
                        this.queryCreator.Errors, false, "Validation failed"));
            }
            else
            {
                var currentUser = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;

                await this.getAllNotesByArgumentUseCase.Handle(
                    new GetAllNotesByArgumentRequest(strategy, currentUser),
                    this.getAllNotesByArgumentPresenter);
            }
            return this.getAllNotesByArgumentPresenter.ContentResult;
        }

        // DELETE api/v1/notes/remove
        [Authorize(AuthenticationSchemes = "Bearer")]
        [Authorize(Roles = "RoleUser")]
        [HttpDelete("{Id}")]
        public async Task<ActionResult> Remove(long Id)
        {
            if (Id <= 0)
                return BadRequest("Not a valid note id");

            var currentUser = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;

            await this.removeNoteUseCase.Handle(new RemoveNoteRequest(Id, currentUser),
                this.removeNotePresenter);
            return this.removeNotePresenter.ContentResult;
        }
    }
}
