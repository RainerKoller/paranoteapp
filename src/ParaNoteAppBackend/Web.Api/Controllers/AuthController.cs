﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Web.Api.Core.Dto.UseCaseRequests;
using Web.Api.Core.Interfaces.UseCases;
using Web.Api.Core.Settings;
using Web.Api.Presenters;

namespace Web.Api.Controllers
{
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ILoginUseCase loginUseCase;
        private readonly IExchangeRefreshTokenUseCase exchangeRefreshTokenUseCase;
        private readonly LoginPresenter loginPresenter;
        private readonly ExchangeRefreshTokenPresenter exchangeRefreshTokenPresenter;
        private readonly AuthSettings authSettings;

        public AuthController(
            [FromServices] ILoginUseCase loginUseCase, 
            [FromServices] IExchangeRefreshTokenUseCase exchangeRefreshTokenUseCase, 
            IOptions<AuthSettings> authsettings)
        {
            this.loginUseCase = loginUseCase;
            this.exchangeRefreshTokenUseCase = exchangeRefreshTokenUseCase;
            this.loginPresenter = new LoginPresenter();
            this.exchangeRefreshTokenPresenter = new ExchangeRefreshTokenPresenter();
            this.authSettings = authsettings.Value;
        }


        // POST api/v1/auth/login
        [HttpPost("Login")]
        public async Task<ActionResult> Login([FromBody] LoginRequest request)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }
            await this.loginUseCase.Handle(new LoginRequest(request.UserName, request.Password, Request.HttpContext.Connection.RemoteIpAddress?.ToString()), this.loginPresenter);
            return this.loginPresenter.ContentResult;
        }

        // POST api/v1/auth/refreshtoken
        [HttpPost("Refreshtoken")]
        public async Task<ActionResult> RefreshToken([FromBody] ExchangeRefreshTokenRequest request)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState);}
            await this.exchangeRefreshTokenUseCase.Handle(new ExchangeRefreshTokenRequest(request.AccessToken, request.RefreshToken, this.authSettings.SecretKey), this.exchangeRefreshTokenPresenter);
            return this.exchangeRefreshTokenPresenter.ContentResult;
        }
    }
}
