﻿using System.Net;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Serialization;

namespace Web.Api.Presenters
{
    public class RemoveNotePresenter : IOutputPort<ManipulateNoteResponse>
    {
        public JsonContentResult ContentResult { get; }
        public RemoveNotePresenter()
        {
            this.ContentResult = new JsonContentResult();
        }

        public void Handle(ManipulateNoteResponse response)
        {
            ContentResult.StatusCode = (int)(response.Success ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            ContentResult.Content = JsonSerializer.SerializeObject(response);
        }
    }
}
