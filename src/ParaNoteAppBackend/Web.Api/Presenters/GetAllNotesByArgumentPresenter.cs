﻿using System.Net;
using Web.Api.Serialization;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;

namespace Web.Api.Presenters
{
    public sealed class GetAllNotesByArgumentPresenter : IOutputPort<GetAllNotesByArgumentResponse>
    {
        public JsonContentResult ContentResult { get; }

        public GetAllNotesByArgumentPresenter()
        {
            this.ContentResult = new JsonContentResult();
        }
        public void Handle(GetAllNotesByArgumentResponse response)
        {
            ContentResult.StatusCode = (int)(response.Success ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            ContentResult.Content = JsonSerializer.SerializeObject(response);
        }
    }
}
