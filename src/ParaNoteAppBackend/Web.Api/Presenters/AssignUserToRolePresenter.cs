﻿using System.Net;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces;
using Web.Api.Serialization;

namespace Web.Api.Presenters
{
    public class AssignUserToRolePresenter : IOutputPort<AssignRoleToUserResponse>
    {
        public JsonContentResult ContentResult { get; }

        public AssignUserToRolePresenter()
        {
            this.ContentResult = new JsonContentResult();
        }

        public void Handle(AssignRoleToUserResponse response)
        {
            this.ContentResult.StatusCode = (int)(response.Success ? HttpStatusCode.OK : HttpStatusCode.BadRequest);
            this.ContentResult.Content = JsonSerializer.SerializeObject(response);
        }
    }
}
