﻿using System;
using System.Collections.Generic;
using System.Linq;
using Web.Api.Core.Dto;
using Web.Api.Core.Interfaces.Strategies;
using Web.Api.Infrastructure.Data.Strategies;

namespace Web.Api.Helpers
{
    public class StrategyCreatorGetNotes
    {
        private Dictionary<string, Type> getNotesByArgumentStrategies;

        public List<Error> Errors { get; private set; }

        public StrategyCreatorGetNotes()
        {
            this.getNotesByArgumentStrategies = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);
            this.Errors = new List<Error>();
            this.Init();
        }

        private void Init() 
        {
            this.getNotesByArgumentStrategies.Add("Done", typeof(GetNoteByDoneStrategy));
        }

        public IGetNotesStrategy CreateNotesQueryStrategy(string key, string value)
        {
            if (IsValid(key))
            {
                if (this.getNotesByArgumentStrategies.TryGetValue(key, out Type requestedStrategy))
                {
                    try
                    {
                        return (IGetNotesStrategy)Activator.CreateInstance(requestedStrategy, value);
                    }
                    catch (FormatException e)
                    {
                        var err = new Error("2", $"Is value {value} valid for given key {key}? Exception {e.Message} was thrown");
                        this.Errors.Add(err);
                        return null;
                    }
                    catch (Exception e)
                    {
                        var err = new Error("3", $"Invokation of query strategy failed? Exception {e.Message} was thrown");
                        this.Errors.Add(err);
                        return null;
                    }
                }
            }
            return null;
        }

        private bool IsValid(string key)
        {
            if (!this.getNotesByArgumentStrategies.Keys
                .ToList<string>()
                .ConvertAll(k => k.ToLower())
                .Contains(key.ToLower()))
            {
                var err = new Error("1", $"No valid key found with given {key}");
                this.Errors.Add(err);
                return false;
            }
            return true;
        }
    }
}
