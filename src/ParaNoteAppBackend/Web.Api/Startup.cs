﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NLog;
using Web.Api.Core;
using Web.Api.Infrastructure;
using Web.Api.Infrastructure.Auth;
using Web.Api.Infrastructure.Data;
using Web.Api.Infrastructure.Identity;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Logging;
using Web.Api.Core.Settings;
using Web.Api.Infrastructure.Helpers;
using Web.Api.Core.Domain.Entities;
using System.Collections.Generic;
using Web.Api.Controllers;
using FluentValidation.AspNetCore;
using Web.Api.DtoModels.Validation;

namespace Web.Api
{
    public class Startup
    {
        private static readonly string ENV_DEBUG = "Debug";
        private static readonly string ENV_PRODUCTION = "Production";
        private static readonly string ENV_TEST = "Test";

        public IConfiguration Configuration { get; }
        public ILifetimeScope AutofacContainer { get; private set; }
        public IWebHostEnvironment CurEnvironment { get; private set; }

        public Startup(IWebHostEnvironment env)
        {
            // Don't try and load nlog config during integ tests.
            string nlogconfigname;
            this.CurEnvironment = env;

            nlogconfigname = $"/nlog.{this.CurEnvironment.EnvironmentName}.config";
            Console.WriteLine($"Environment is: {this.CurEnvironment.EnvironmentName}");

            if (!this.CurEnvironment.EnvironmentName.Equals(ENV_TEST))
            {
                var nLogConfigPath = string.Concat(Directory.GetCurrentDirectory(), nlogconfigname);
                if (File.Exists(nLogConfigPath)) { LogManager.LoadConfiguration(string.Concat(Directory.GetCurrentDirectory(), nlogconfigname)); }
            }

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();

            this.Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services
                .AddMvc()
                .AddControllersAsServices()
                .AddFluentValidation(mvcConfig => 
                    mvcConfig.RegisterValidatorsFromAssemblyContaining<RegisterUserRequestValidator>()); // any validator in that assembly

            //Register the ConfigurationBuilder instance of AuthSettings
            var authSettings = Configuration.GetSection(nameof(AuthSettings));
            services.Configure<AuthSettings>(authSettings);

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(
                authSettings[nameof(AuthSettings.SecretKey)]));

            // jwt wire up
            // Get options from app settings
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            // Configure JwtIssuerOptions
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(configureOptions =>
            {
                configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                configureOptions.TokenValidationParameters = tokenValidationParameters;
                configureOptions.SaveToken = true;

                configureOptions.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

            var connectionstring = Configuration.GetConnectionString("Default");

            if (this.CurEnvironment.EnvironmentName.Equals(ENV_DEBUG))
            {

                Console.WriteLine("Executing Directory: " + Directory.GetCurrentDirectory());
                
                var dbfile = Path.GetFileName(connectionstring);
                var dbFileWithPathFromPublish = MoveDirectoryUp(8) + Path.DirectorySeparatorChar + "sql" + Path.DirectorySeparatorChar + dbfile;
                var dbFileWithPathFromDebug = MoveDirectoryUp(3) + Path.DirectorySeparatorChar + "sql" + Path.DirectorySeparatorChar + dbfile;


                if (File.Exists(dbFileWithPathFromPublish))
                {
                    connectionstring = $"Data Source={Path.GetFullPath(dbFileWithPathFromPublish)}";
                }
                else if (File.Exists(dbFileWithPathFromDebug))
                {
                    connectionstring = $"Data Source={Path.GetFullPath(dbFileWithPathFromDebug)}";
                }

            }
            if (! this.CurEnvironment.EnvironmentName.Equals(ENV_TEST))
            {
                Console.WriteLine("Database connection string: " + connectionstring);

                services.AddDbContext<AppIdentityDbContext>(options => options.UseSqlite(connectionstring, b => b.MigrationsAssembly("Web.Api.Infrastructure")));
                services.AddDbContext<AppDbContext>(options => options.UseSqlite(connectionstring, b => b.MigrationsAssembly("Web.Api.Infrastructure")));
            }

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<AppIdentityDbContext>();

            var identityBuilder = services.AddIdentityCore<AppUser>(o =>
            {
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 6;
            });

            identityBuilder = new IdentityBuilder(identityBuilder.UserType, typeof(IdentityRole), identityBuilder.Services);
            identityBuilder.AddEntityFrameworkStores<AppIdentityDbContext>().AddDefaultTokenProviders();

            services.AddApiVersioning();

            if (!this.CurEnvironment.EnvironmentName.Equals("Test"))
            { 
                services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            }
            else
            {
                var assembly = typeof(Program).GetTypeInfo().Assembly;
                services.AddAutoMapper(cfg =>
                {
                    cfg.AllowNullDestinationValues = true;
                    cfg.CreateMap<User, AppUser>().ConstructUsing(u => new AppUser { UserName = u.UserName, Email = u.Email }).ForMember(au => au.Id, opt => opt.Ignore());
                    cfg.CreateMap<AppUser, User>().ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email)).
                                               ForMember(dest => dest.PasswordHash, opt => opt.MapFrom(src => src.PasswordHash)).
                                               ForAllOtherMembers(opt => opt.Ignore());
                }, assembly);
            }

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Paranote App", Version = "v1" });
                //Swagger 2.+ support
                c.AddSecurityDefinition("Bearer", //Name the security scheme
                    new OpenApiSecurityScheme {
                    Description = "JWT Authorization header using the Bearer scheme.",
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement 
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Id = "Bearer", //The name of the previously defined security scheme.
                                Type = ReferenceType.SecurityScheme
                            }
                        }, new List<string>() 
                    }
                });
            });

            // Now register our services with Autofac container.
            var builder = new ContainerBuilder();
            
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Controller", StringComparison.Ordinal)).PropertiesAutowired();

            builder.Populate(services);
            var container = builder.Build();
        }

        public void ConfigureContainer(ContainerBuilder builder) 
        {
            builder.RegisterModule(new CoreModule());
            builder.RegisterModule(new InfrastructureModule());

            builder.RegisterType<AccountsController>().PropertiesAutowired();
            builder.RegisterType<AuthController>().PropertiesAutowired();
            builder.RegisterType<NotesController>().PropertiesAutowired();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {

            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            //loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();
            //app.UseMvc();

            //app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PaRa Note App V1");
            });

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();


            // keep order of 
            //  1) UseRouting(), 
            //  2) UseAuthentication() 
            //  3) UseAuthorization() 
            //  4) UseEndpoints()
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }

        private static string MoveDirectoryUp(int nr = 1)
        {
            string ret = string.Empty;
            for (int i = 0; i < nr; ++i)
            {
                ret += ".." + Path.DirectorySeparatorChar;
            }
            return ret;
        }
    }
}
