﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;

namespace Web.Api
{
    public class Program
    {


        public static void Main(string[] args)
        {

            IHost host;

            var env = "Debug";

            if ((Debugger.IsAttached) || (args is null))
            {
                env = "Debug";
            }
            else
                try
            {
                if (0 == String.Compare(args[0], "--Production", StringComparison.OrdinalIgnoreCase))
                {
                    env = "Production";
                }
            }
            catch (IndexOutOfRangeException e) 
            {
                env = "Debug";
            }
            host = Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webHostBuilder =>
                {
                    webHostBuilder
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseEnvironment(env)
                    .UseStartup<Startup>();
                })
                .Build();
            host.Run();
        }
    }
}
