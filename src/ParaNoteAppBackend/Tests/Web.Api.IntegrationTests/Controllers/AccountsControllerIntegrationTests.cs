﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web.Api.Core.Dto.UseCaseRequests;
using Xunit;

namespace Web.Api.IntegrationTests.Controllers
{
    public class AccountsControllerIntegrationTests 
        : IClassFixture<GenericWebApplicationFactory<Startup, AccountsControllerSeedData>>
    {
        private readonly HttpClient client;
        private readonly GenericWebApplicationFactory<Startup, AccountsControllerSeedData> factory;

        public AccountsControllerIntegrationTests(GenericWebApplicationFactory<Startup, AccountsControllerSeedData> factory)
        {
            this.factory = factory;
            this.client = factory.CreateClient();
        }

        [Fact]
        public async Task CanRegisterUserWithValidAccountDetails()
        {
            var token = await this.GetTokenAsync("Administrator", "Pa$$W0rd1");

            this.client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            var httpResponse = await this.client.PostAsync("/api/v1/Accounts/Register", 
                new StringContent(JsonConvert.SerializeObject(
                    new RegisterUserRequest("Milford", "Ahmed", "ahmed@email.com", "Milford", "S0unD44ç")), 
                    Encoding.UTF8, "application/json"));
            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            dynamic result = JObject.Parse(stringResponse);
            Assert.True((bool) result.success);
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        [Fact]
        public async Task CantRegisterUserWithInvalidAccountDetails()
        {
            var token = await this.GetTokenAsync("Administrator", "Pa$$W0rd1");

            this.client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            var httpResponse = await this.client.PostAsync("/api/v1/Accounts/Register", 
                new StringContent(JsonConvert.SerializeObject(
                    new RegisterUserRequest("Milford", "Ahmed", "ahmed@email.com", "Milford", "Pa$$word1")), 
                    Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Contains("User name 'Milford' is already taken", stringResponse);
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

        [Fact]
        public async Task CanAssignUserToRole()
        {
            var token = await this.GetTokenAsync("Administrator", "Pa$$W0rd1");

            this.client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            var httpResponse = await this.client.PostAsync("/api/v1/Accounts/AssignRole",
                new StringContent(JsonConvert.SerializeObject(
                    new AssignRoleToUserRequest("NotesViewer", "RoleUser")),
                    Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        private async Task<string> GetTokenAsync(string user, string password)
        {
            HttpClient loginClient;

            loginClient = this.factory.CreateClient();

            var httpResponseLogin = await loginClient.PostAsync("/api/v1/Auth/Login", 
                new StringContent(JsonConvert.SerializeObject(
                    new LoginRequest { UserName = user, Password = password }), Encoding.UTF8, "application/json"));
            httpResponseLogin.EnsureSuccessStatusCode();
            var stringResponseLogin = await httpResponseLogin.Content.ReadAsStringAsync();

            dynamic resultLogin = JObject.Parse(stringResponseLogin);

            return resultLogin.accessToken.token.ToString();
        }
    }
}

 

