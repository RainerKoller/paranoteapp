﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseRequests;
using Xunit;

namespace Web.Api.IntegrationTests.Controllers
{
    public class NotesControllerIntegrationTests 
        : IClassFixture<GenericWebApplicationFactory<Startup, NotesControllerSeedData>>

    {

        private readonly HttpClient client;
        private readonly GenericWebApplicationFactory<Startup, NotesControllerSeedData> factory;

        public NotesControllerIntegrationTests(GenericWebApplicationFactory<Startup, NotesControllerSeedData> factory)
        {
            this.factory = factory;
            this.client = this.factory.CreateClient();
        }

        [Fact]
        public async Task CandAddNoteAuthorized()
        {
            string token = await this.GetTokenAsync("NotesWriter", "Pa$$W0rd1");

            this.client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

            var httpResponse = await this.client.PostAsync("/api/v1/notes",
                new StringContent(JsonConvert.SerializeObject(
                    new DtoAddNoteRequest("MyTitle", "my description", new System.DateTime(), 1)),
                    Encoding.UTF8, "application/json"));

            httpResponse.EnsureSuccessStatusCode();
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            dynamic result = JObject.Parse(stringResponse);
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
        }

        private async Task<string> GetTokenAsync(string user, string password)
        {
            HttpClient loginClient;

            loginClient = this.factory.CreateClient();

            var httpResponseLogin = await loginClient.PostAsync("/api/v1/auth/login",
                new StringContent(JsonConvert.SerializeObject(
                    new LoginRequest
                    {
                        UserName = user,
                        Password = password
                    }), Encoding.UTF8, "application/json"));
            httpResponseLogin.EnsureSuccessStatusCode();
            var stringResponseLogin = await httpResponseLogin.Content.ReadAsStringAsync();

            dynamic resultLogin = JObject.Parse(stringResponseLogin);

            return resultLogin.accessToken.token.ToString();
        }
    }
}
