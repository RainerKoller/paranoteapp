﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Web.Api.Core.Domain.Entities;
using Web.Api.Infrastructure.Data;
using Web.Api.Infrastructure.Identity;

namespace Web.Api.IntegrationTests
{

    public abstract class BasicSeedData : ISeedDataClass
    {
        private static string PathToStimuli 
        { 
            get 
            { 
                return MoveDirectoryUp(7) + Path.Combine("Validation", "InputStimuli");
            }
        }
        
        private static readonly string PATH_IDENTITY = Path.Combine(PathToStimuli, "GenericIdentityModels");
        private readonly string PathData;

        private IMapper iMapper;

        public BasicSeedData(string stimuli)
        {
            this.PathData = Path.Combine(PathToStimuli, stimuli);
        }
        public virtual void InitializeDbForTests(AppIdentityDbContext ictxt, AppDbContext actxt)
        {
            var appusers = JsonConvert.DeserializeObject<List<AppUser>>(File.ReadAllText(
                Path.Combine(PATH_IDENTITY, "Setup_IdentityAppUsers.json")));
            foreach (var appuser in appusers)
                ictxt.Users.Add(appuser);

            var approles = JsonConvert.DeserializeObject<List<IdentityRole>>(File.ReadAllText(
                Path.Combine(PATH_IDENTITY, "Setup_IdentityIdentityRoles.json")));
            foreach (var role in approles)
                ictxt.Roles.Add(role);

            var appuserroles = JsonConvert.DeserializeObject<List<IdentityUserRole<string>>>(File.ReadAllText(
                Path.Combine(PATH_IDENTITY, "Setup_IdentityIdentityUserRoles.json")));
            foreach (var appuserrole in appuserroles)
                ictxt.UserRoles.Add(appuserrole);

            ictxt.SaveChanges();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AppUser, User>()
                    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                    .ForMember(dest => dest.PasswordHash, opt => opt.MapFrom(src => src.PasswordHash))
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                    .ForMember(dest => dest.IdentityId, opt => opt.MapFrom(src => src.Id))
                    .ForAllOtherMembers(opt => opt.Ignore());
            });

            this.iMapper = config.CreateMapper();

            var users = this.iMapper.Map<List<AppUser>, List<User>>(appusers);

            var tokens = JsonConvert.DeserializeObject<List<RefreshToken>>(File.ReadAllText(
                Path.Combine(PATH_IDENTITY, "Setup_UsersRefreshToken.json")));

            var userid = 0;
            foreach (User user in users)
            {
                user.Id = ++userid;
                var token = tokens.Where(t => t.UserId == user.Id).FirstOrDefault();
                user.AddRefreshToken(token.Token, token.UserId, token.RemoteIpAddress);
                actxt.Users.Add(user);
            }

            var notes = JsonConvert.DeserializeObject<Note>(File.ReadAllText(Path.Combine(
                this.PathData, "Setup_Notes.json")));
            actxt.Notes.Add(notes);

            actxt.SaveChanges();
        }

        private static string MoveDirectoryUp(int nr = 1) 
        {
            string ret = string.Empty;
            for (int i = 0; i < nr; ++i) 
            {
                ret += ".." + Path.DirectorySeparatorChar;
            }
            return ret;
        }
    }

    public class AccountsControllerSeedData : BasicSeedData
    {
        private static readonly string PATH_DATA = "AccountsControllerIntegrationTest";
        public AccountsControllerSeedData() : base(PATH_DATA) { }
    }

    public class AuthControllerSeedData : BasicSeedData
    {
        private static readonly string PATH_DATA = "AuthControllerIntegrationTest";
        public AuthControllerSeedData() : base(PATH_DATA) { }
    }

    public class NotesControllerSeedData : BasicSeedData
    {
        private static readonly string PATH_DATA = "NotesControllerIntegrationTest";
        public NotesControllerSeedData() : base(PATH_DATA) { }
    }
}
