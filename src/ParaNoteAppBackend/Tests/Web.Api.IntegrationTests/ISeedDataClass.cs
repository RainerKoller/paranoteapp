﻿using System;
using Web.Api.Infrastructure.Data;
using Web.Api.Infrastructure.Identity;

namespace Web.Api.IntegrationTests
{
    public interface ISeedDataClass
    {
        void InitializeDbForTests(AppIdentityDbContext ictxt, AppDbContext actxt);
    }
}
