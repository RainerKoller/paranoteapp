﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Web.Api.Core.Dto;
using Web.Api.Core.Interfaces.Services;
using Web.Api.Infrastructure.Interfaces;

namespace Web.Api.Infrastructure.Auth
{
    internal sealed class JwtFactory : IJwtFactory
    {
        private readonly IJwtTokenHandler jwtTokenHandler;
        private readonly JwtIssuerOptions jwtOptions;

        internal JwtFactory(IJwtTokenHandler jwtTokenHandler, IOptions<JwtIssuerOptions> jwtOptions)
        {
            this.jwtTokenHandler = jwtTokenHandler;
            this.jwtOptions = jwtOptions.Value;
            ThrowIfInvalidOptions(this.jwtOptions);
        }

        public async Task<AccessToken> GenerateEncodedToken(string id, string userName, List<string> roles)
        {
            var identity = GenerateClaimsIdentity(id, userName, roles);

            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, userName));
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, await this.jwtOptions.JtiGenerator()));
            claims.Add(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(this.jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64));
            foreach (var claim in identity.Claims.Where(c => c.Type == Helpers.Constants.Strings.JwtClaimIdentifiers.Roles)) 
            {
                claims.Add(claim);
            }
            claims.Add(identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Id));

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                this.jwtOptions.Issuer,
                this.jwtOptions.Audience,
                claims,
                this.jwtOptions.NotBefore,
                this.jwtOptions.Expiration,
                this.jwtOptions.SigningCredentials);
          
            return new AccessToken(this.jwtTokenHandler.WriteToken(jwt), (int)this.jwtOptions.ValidFor.TotalSeconds);
        }

        private static ClaimsIdentity GenerateClaimsIdentity(string id, string userName, List<string> roles)
        {

            var ci = new ClaimsIdentity(new GenericIdentity(userName, "Token"));

            ci.AddClaim(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Id, id));

            foreach (var role in roles)
            {
                switch (role)
                {
                    case Helpers.Constants.Strings.JwtClaims.RoleAdmin:
                        ci.AddClaim(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Roles, Helpers.Constants.Strings.JwtClaims.RoleAdmin));
                        break;
                    case Helpers.Constants.Strings.JwtClaims.RoleUser:
                        ci.AddClaim(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Roles, Helpers.Constants.Strings.JwtClaims.RoleUser));
                        break;
                    case Helpers.Constants.Strings.JwtClaims.RoleViewer:
                        ci.AddClaim(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Roles, Helpers.Constants.Strings.JwtClaims.RoleViewer));
                        break;
                    default:
                        ci.AddClaim(new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Roles, Helpers.Constants.Strings.JwtClaims.RoleUser));
                        break;
                }
            }

            return ci;
        }

        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
        private static long ToUnixEpochDate(DateTime date)
          => (long)Math.Round((date.ToUniversalTime() -
                               new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                              .TotalSeconds);

        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtIssuerOptions.ValidFor));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
            }

            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
            }
        }
    }
}
