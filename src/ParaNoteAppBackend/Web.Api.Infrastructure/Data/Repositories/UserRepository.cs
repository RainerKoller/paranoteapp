﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.GatewayResponses.Repositories;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Specifications;
using Web.Api.Infrastructure.Identity;


namespace Web.Api.Infrastructure.Data.Repositories
{
    internal sealed class UserRepository : EfRepository<User>, IUserRepository
    {
        private readonly UserManager<AppUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IMapper iMapper;


        public UserRepository(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager, 
            IMapper mapper, AppDbContext appDbContext): base(appDbContext)
        {
            this.userManager = userManager;
            this.iMapper = mapper;
            this.roleManager = roleManager;
        }

        public async Task<CreateUserResponse> Create(string firstName, string lastName, string email, string userName, string password)
        {
            var appUser = new AppUser {Email = email, UserName = userName};
            var identityResult = await this.userManager.CreateAsync(appUser, password);

            if (!identityResult.Succeeded) return new CreateUserResponse(
                appUser.Id, false, identityResult.Errors.Select(e => new Error(e.Code, e.Description)));
          
            var user = new User(firstName, lastName, appUser.Id, appUser.UserName);
            this.appDbContext.Users.Add(user);
            await this.appDbContext.SaveChangesAsync();

            return new CreateUserResponse(appUser.Id, identityResult.Succeeded, identityResult.Succeeded ? null : identityResult.Errors.Select(e => new Error(e.Code, e.Description)));
        }

        public async Task<AssignRoleToUserResponse> AssignRole(string userName, string nameRole) 
        {
            bool roleExists = false;
            List<Error> errs = null;

            foreach (var rolename in Helpers.Constants.Strings.JwtClaims.GetAllRoleNames())
            {
                if (0 == nameRole.CompareTo(rolename))
                {
                    roleExists = true;
                    break;
                }
            }

            if (!roleExists)
            {
                errs = new List<Error>();
                errs.Add(new Error("1", $"Role {nameRole} does not exist"));
                return new AssignRoleToUserResponse(errs);
            }

            var user = await this.userManager.FindByNameAsync(userName);

            if (user is null)
            {
                errs = new List<Error>();
                errs.Add(new Error("2", $"User {userName} does not exist"));
                return new AssignRoleToUserResponse(errs);
            }

            var roleResult = await this.userManager.AddToRoleAsync(user, nameRole);

            if (!roleResult.Succeeded) return new AssignRoleToUserResponse(roleResult.Errors.Select(e => new Error(e.Code, e.Description)));

            return new AssignRoleToUserResponse(true);
        }

        public async Task<User> FindByName(string userName)
        {
            var appUser = await this.userManager.FindByNameAsync(userName);
            return appUser == null ? null : this.iMapper.Map(appUser, await GetSingleBySpec(new UserSpecification(appUser.Id)));
        }

        public async Task<bool> CheckPassword(User user, string password)
        {
            return await this.userManager.CheckPasswordAsync(this.iMapper.Map<AppUser>(user), password);
        }


        public async Task<List<string>> GetUsersRoles(User user) 
        {
            var myApaUser  = await this.userManager.FindByNameAsync(user.UserName);
            var uroles = await this.userManager.GetRolesAsync(myApaUser);
            return uroles.ToList<string>();
        }

    }
}
