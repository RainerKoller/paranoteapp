﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Shared;

namespace Web.Api.Infrastructure.Data.Repositories
{

    public abstract class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly AppDbContext appDbContext;

        protected EfRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public virtual async Task<T> GetById(long id)
        {
            return await appDbContext.Set<T>().FindAsync(id);
        }

        public async Task<List<T>> ListAll()
        {
            return await appDbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetSingleBySpec(ISpecification<T> spec)
        {
            var result = await List(spec);
            return result.FirstOrDefault();
        }

        public async Task<List<T>> List(ISpecification<T> spec)
        {
            // fetch a Queryable that includes all expression-based includes
            var queryableResultWithIncludes = spec.Includes
                .Aggregate(appDbContext.Set<T>().AsQueryable(),
                    (current, include) => current.Include(include));

            // modify the IQueryable to include any string-based include statements
            var secondaryResult = spec.IncludeStrings
                .Aggregate(queryableResultWithIncludes,
                    (current, include) => current.Include(include));

            // return the result of the query using the specification's criteria expression
            return await secondaryResult
                            .Where(spec.Criteria)
                            .ToListAsync();
        }


        public async Task<T> Add(T entity)
        {
            appDbContext.Set<T>().Add(entity);
            await appDbContext.SaveChangesAsync();
            return entity;
        }

        public async Task Update(T entity)
        {
            appDbContext.Entry(entity).State = EntityState.Modified;
            await appDbContext.SaveChangesAsync();
        }

        public async Task Delete(T entity)
        {
            appDbContext.Set<T>().Remove(entity);
            await appDbContext.SaveChangesAsync();
        }
    }
}
