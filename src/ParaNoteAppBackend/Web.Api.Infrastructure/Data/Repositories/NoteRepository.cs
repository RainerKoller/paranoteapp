﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Dto;
using Web.Api.Core.Dto.UseCaseResponses;
using Web.Api.Core.Interfaces.Gateways.Repositories;
using Web.Api.Core.Interfaces.Strategies;
using Web.Api.Infrastructure.Identity;

namespace Web.Api.Infrastructure.Data.Repositories
{
    public class NoteRepository : EfRepository<Note>, INoteRepository
    {

        private readonly UserManager<AppUser> userManager;
        private readonly IMapper mapper;

        public NoteRepository(UserManager<AppUser> userManager, 
            IMapper mapper, AppDbContext dbcontext) : base(dbcontext)
        {
            this.mapper = mapper;
            this.userManager = userManager;
        }

        public async Task<ManipulateNoteResponse> Add(string title, string description, DateTime duedate, int importance, long userId)
        {
            var note = new Note(title, description, duedate, importance);
            this.appDbContext.Notes.Add(note);
            var nrEntries = await this.appDbContext.SaveChangesAsync();

            if (1 == nrEntries)
                return new ManipulateNoteResponse(note.Id, true);

            var errs = new List<Error>();

            if (0 == nrEntries)
            {
                errs.Add(new Error("2", "Note could not be storeded. Please try again"));
                return new ManipulateNoteResponse(errs, false, "System failure");
            }

            errs.Add(new Error("3", "More than one Note stored. Please contact SysAdmin"));
            return new ManipulateNoteResponse(errs, false, "System failure");
        }

        public async Task<List<Note>> GetByStrategy(IGetNotesStrategy strategy, User user)
        {
            try
            {
                return await strategy.GetNotes(this.appDbContext, user);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<List<Note>> GetByUser(User user)
        {
            try
            {
                var ctxt = this.appDbContext;

                if (ctxt.Users.Contains(user))
                {
                    return ctxt.Users
                        .Include(n => n.Notes)
                        .Where(u => u.Id == user.Id)
                        .ToList()
                        .SelectMany(note => note.Notes)
                        .ToList();
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<ManipulateNoteResponse> Update(long id, string title, string description, DateTime duedate, int importance, bool done)
        {
            var errs = new List<Error>();
            var note = await this.GetById(id);

            if (null == note)
            {
                errs.Add(new Error("-1", "Note could not be resolved."));
                return new ManipulateNoteResponse(errs, false, "System failure");
            }

            note.Title = title;
            note.Description = description;
            note.DueDate = duedate;
            note.Importance = importance;
            note.Done = done;

            if (note.Done)
                note.DoneDate = DateTime.Now;
            else 
                note.DoneDate = null;

            await this.Update(note);
            return new ManipulateNoteResponse(note.Id, true);
        }
    }
}
