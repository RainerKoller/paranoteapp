﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Api.Core.Domain.Entities;
using Web.Api.Core.Interfaces.Strategies;

namespace Web.Api.Infrastructure.Data.Strategies
{
    public class GetNoteByDoneStrategy : IGetNotesStrategy
    {
        private readonly bool done;

        public GetNoteByDoneStrategy(string done)
        {
            try
            {
                this.done = Convert.ToBoolean(done);
            }
            catch (Exception e) 
            {
                this.done = false; // ToDo: implement better error handling
            }
        }

        public async Task<List<Note>> GetNotes(object dbcontext, User user)
        {
            if (dbcontext is AppDbContext)
            {
                var ctxt = (AppDbContext)dbcontext;

                if (ctxt.Users.Contains(user))
                {
                    return ctxt.Users
                        .Include(n => n.Notes)
                        .Where(u => u.Id == user.Id)
                        .ToList()
                        .SelectMany(note => note.Notes)
                        .Where(n => n.Done == this.done)
                        .ToList();
                }
            }
            return null;
        }
    }
}
