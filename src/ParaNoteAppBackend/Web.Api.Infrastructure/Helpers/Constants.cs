﻿

using System.Collections.Generic;

namespace Web.Api.Infrastructure.Helpers
{
    public static class Constants
    {
        public static class Strings
        {
            public static class JwtClaimIdentifiers
            {
                public const string Roles = "roles", Id = "id";
            }

            public static class JwtClaims
            {
                public static List<string> GetAllRoleNames() 
                {
                    // do not expose "RoleAdmin" - we should not allow to create user with this role
                    return new List<string>() { RoleUser, RoleViewer };
                }

                public const string RoleUser = "RoleUser";
                public const string RoleAdmin = "RoleAdmin";
                public const string RoleViewer = "RoleViewer ";
            }
        }
  }
}
