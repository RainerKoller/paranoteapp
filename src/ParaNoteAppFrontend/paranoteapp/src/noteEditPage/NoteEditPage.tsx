import React, { Component, ReactHTML } from "react";
import './NoteEditPage.css';
import '../../src/components/applayout/applayout.css' 
import '../components/UIComponents/buttons.css'
import ImportanceSelector from '../components/UIComponents/ImportanceSelector/ImportanceSelector'
import { Redirect, useHistory } from 'react-router-dom';
import BackendService from '../services/BackendService'
import { ParseNotes, NotesResponse, Note } from '../common/Note'
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


export enum EditNotePageMode {
    Editing = 0,
    CreateNew = 1
}

function convertToUTC(date: Date) : Date {
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
}


export class EditNotePageModeUtil {
    static fromModeToString(mode: EditNotePageMode): string {
        switch (mode) {
          case EditNotePageMode.CreateNew:
            return "0";
          case EditNotePageMode.Editing:
            return "1";
        }
      }

    static fromStringToMode(x: string | null): EditNotePageMode {
        switch (x) {
          case "0":
            return EditNotePageMode.CreateNew;
      
          case "1":
            return EditNotePageMode.Editing;
      
          default:
            return EditNotePageMode.CreateNew;
        }
      }
}

interface NoteEditPageState {
    mode: EditNotePageMode
    note?: Note
    toNotesPage: boolean;
    dueDateUTC: Date;
    darkModeOn: boolean;
    errorMessage: string;
}
interface NoteEditPageProps {

}

export default class NoteEditPage extends React.Component<NoteEditPageProps, NoteEditPageState> { 
    constructor(props: NoteEditPageProps) {
        super(props)
    
        this.state = {
            mode: EditNotePageMode.CreateNew,
            toNotesPage: false,
            dueDateUTC: new Date(),
            darkModeOn: this.getDarkModePreference(),
            errorMessage: ""
        }
    }

    async componentDidMount() {
        await this.getNoteFromLocalStorage();
        await this.getModeFromLocalStorage();
        await this.setEmptyTempNoteIfNull();
    }

    private getDarkModePreference(): boolean {
        const darkModeOn = localStorage.getItem('darkMode')
        return darkModeOn == "on"
    }

    private async getNoteFromLocalStorage() { 
        const noteFromLocalStorage: string | null = localStorage.getItem('note') 
        if (noteFromLocalStorage != null) {
            const parsedNote = JSON.parse(noteFromLocalStorage as string) as Note
            await this.setState({
                note : parsedNote,
                //dueDateUTC : ConvertToUTC(parsedNote.dueDate) this does not work for some reason
            })
        }
    }

    private async storeCurrentNoteInLocalStorage() {
        const noteJson = JSON.stringify(this.state.note)
        localStorage.setItem('note', noteJson);
    }

    private async getModeFromLocalStorage() { 
        const modeFromLocalStorage = localStorage.getItem('mode');
        await this.setState({
            mode : EditNotePageModeUtil.fromStringToMode(modeFromLocalStorage)
        }) 
    }

    private async setEmptyTempNoteIfNull() {
        if (this.state.note == null) {
            await this.setState( {
                note: {
                    id : 0,
                    title : "",
                    description: "",
                    dueDate: new Date(),
                    importance: 0,
                    done: false,
                    createdDate: new Date()
                }
            })
        }
    }

    private handleDateChange = async (date: Date) => {
        console.log(date)
        let tempNote = this.state.note
        if (tempNote != null) {
            tempNote.dueDate = convertToUTC(date)
            this.setState({
                dueDateUTC: convertToUTC(date),
                note: tempNote
            }) 
        }
    }

    private validateForm(): boolean {
        let error = "Following Fields are still required: "
        if (this.state.note?.title == "") {
            error += "Title"
        }
        if (this.state.note?.description == "") {
            error += error.includes("Title") ? ", Description" : "Description"
        }
        if (this.state.note?.importance == null || this.state.note?.importance == 0) {
            if (error.includes("Title") || error.includes("Description")) {
                error += ", Importance"
            }
            else {
                error += "Importance"
            }
        }
        if (error == "Following Fields are still required: ") {
            return true
        }
        this.setState({errorMessage: error}) 
        return false
    }

    private onSaveTapped = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        event.preventDefault();
        if (this.validateForm()) {  
            switch (this.state.mode) {
                case EditNotePageMode.CreateNew: {
                    if (this.state.note != null) {
                        await BackendService.createNew(this.state.note!)
                        this.navigateToNotesPage();
                    }
                    break;
                }
                case EditNotePageMode.Editing: {
                    if (this.state.note != null) {
                        await BackendService.updateExisting(this.state.note!)
                        this.navigateToNotesPage();
                    }
                    break;
                }
            }
        }
    }

    private onCancelTapped = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        event.preventDefault();
        this.navigateToNotesPage()
    }

    private onDeleteTapped = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        event.preventDefault();
        if (this.state.note != null) { 
            await BackendService.deleteNote(this.state.note)
            this.navigateToNotesPage()
        }
    }

    private navigateToNotesPage = () => {
        this.setState({toNotesPage: true});
    }

    private async titleChanged(event: React.ChangeEvent<HTMLInputElement>) {
        let tempNote : Note
        if (this.state.note == null) {
            tempNote = {
                id : 0,
                title : "",
                description: "",
                dueDate: new Date(),
                importance: 0,
                done: false,
                createdDate: new Date()
            }
        } else {
            tempNote = this.state.note!
        }
        tempNote.title = event.target.value

        await this.setState({note : tempNote})
        this.storeCurrentNoteInLocalStorage()
    }

    private async descriptionChanged(event: React.ChangeEvent<HTMLTextAreaElement>){
        let tempNote : Note
        tempNote = this.state.note!
        tempNote.description = event.target.value
        await this.setState({note : tempNote})
        console.log(this.state)
        this.storeCurrentNoteInLocalStorage()
    }

    private onImportanceChanged = async (toValue: number) => {
        const tempNote = this.state.note
        if (tempNote != null) {
            tempNote!.importance = toValue
            await this.setState({note: tempNote})
            console.table(tempNote)
            this.storeCurrentNoteInLocalStorage()
        }
    }

    render() {
        const note = this.state.note
        let deleteButton = this.state.mode != EditNotePageMode.Editing ? null : (
            <div className="pna__deleteButtonContainer">
                <button onClick={this.onDeleteTapped} type="submit" className="deleteButton">Delete</button>
            </div>
        )
        if (this.state.toNotesPage) {
            return(
                <Redirect to='/notes' />
            )
        } else {
            return (
                <div className={this.state.darkModeOn ? 'darkTheme' : ''}>
                    <div className="pna__noteedit_container">
                        <div className={"background-image"}/>
                        <div className="errorMessage">{this.state.errorMessage}</div>
                        {deleteButton}
                        <input type="text" onChange={(event) => {this.titleChanged(event)}} placeholder="Title" value={note?.title} className="titleField"></input>
                        <textarea  placeholder="Description" value= {note?.description} onChange={ (event) => {this.descriptionChanged(event)}} className="descriptionField"></textarea>
                        <ImportanceSelector interactive initImportance={note?.importance} minImportance={0} maxImportance={5} onChangedSelection={this.onImportanceChanged}/>
                        <div className="dateContainer">
                            <div className="dateTitle">Date: </div>
                            <DatePicker className="dateField" selected={this.state.dueDateUTC} onChange={this.handleDateChange} dateFormat="MM/dd/yyyy"></DatePicker>
                        </div>
                        <div className="buttonsContainer">
                            <button onClick={this.onCancelTapped} type="submit" className="cancelButton">Cancel</button>
                            <button onClick={this.onSaveTapped} type="submit" className="button">Save</button>
                        </div>              
                    </div>
                </div>
            );
        }
    }
}