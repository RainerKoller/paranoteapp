import React, { Component, useState } from "react";
import './LoginPage.css';
import '../../src/components/applayout/applayout.css' 
import '../components/UIComponents/buttons.css'
//©import '../../src/components/notelistitem/notelistitem.css'
import { Redirect, useHistory } from 'react-router-dom';
import { ParseTokenStructure } from './TokenStructure'
import { render } from '@testing-library/react';
import PropTypes from "prop-types";
import BackendService from "../services/BackendService";


interface LoginState {
    toNotesPage: boolean;
    userName: string;
    password: string;
    loginSuccesful: boolean;
    darkModeOn: boolean
    errorMessage: string
}

interface IFormProps {

}


export default class LoginPage extends React.Component<IFormProps, LoginState> {
  constructor(props: IFormProps) {
    super(props)

    this.state = {
        toNotesPage: false,
        userName: "",
        password: "",
        loginSuccesful: false,
        darkModeOn: this.getDarkModePreference(),
        errorMessage: ""
    }
  }

  private getDarkModePreference(): boolean {
    const darkModeOn = localStorage.getItem('darkMode')
    return darkModeOn == "on"
  }


  private validateForm(): boolean {
    return this.state.userName != "" && this.state.password != "";
  }

  private navigateToNotesPage = () => {
    this.setState({toNotesPage: true});
  }

  private handleSubmit = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();

    if (this.validateForm()) {     
      BackendService.login(this.state.userName, this.state.password, 
          //Success
          () => {
            this.navigateToNotesPage();
            this.setState({errorMessage: ""})
          }, 
          //Fail
          () => {
            this.setState({errorMessage: "Something went wrong! Please verify your username and password!"})
          }
        )
    }
    else {
      this.setState({errorMessage: "Make sure you enter a username and password!"})
    }
  };

  private passwordChanged(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({password: event.target.value})
    console.log(this.state)
  }

  private userNameChanged(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({userName: event.target.value})
    console.log(this.state)
  }
  

  public render() {
    if (this.state.toNotesPage) {
        return(
            <Redirect to='/notes' />
        )
    }
    return (
      <div className={this.state.darkModeOn ? 'darkTheme' : ''}>
        <form onSubmit={this.handleSubmit} noValidate={true}>
          <div className="background-image"></div>
          <div className="LoginPage">
            <div className="title">ParaNoteApp</div>
            <div className="container">
              <div className="loginInputsContainer">
                    <div className="errorMessage">{this.state.errorMessage}</div>
                    <input type="text" onChange={(event) => this.userNameChanged(event)} placeholder="Username" className="loginField"></input>
                    <input type="password" onChange={(event) => this.passwordChanged(event)} placeholder="Password" className="loginField"></input>
                    <button type="submit" className="button">Login</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}