import { ParseNotes, NotesResponse, Note } from '../common/Note'
import { ParseTokenStructure } from '../loginPage/TokenStructure';


export default class BackendService {
    public static createNew = async (note: Note) => {
        let tmpDueDate = new Date(note.dueDate)
        let tokenFromStorage = localStorage.getItem("token");
        const response = await fetch('/api/v1/notes', {
            method: 'POST',
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization' : `Bearer ${tokenFromStorage}`
               }),
           body: `{\"title\": \"${note.title}\", 
                    \"description\": \"${note?.description}\",
                    \"dueDate\": \"${tmpDueDate.toISOString()}\",
                    \"importance\": ${note.importance}
                }`
        })
        console.table(response);
    }

    public static deleteNote = async (note: Note) => {
        let tokenFromStorage = localStorage.getItem("token");
        const response = await fetch(`/api/v1/notes/${note.id}`, {
            method: 'DELETE',
            headers: new Headers({
                'content-type': 'application/json',
                'Authorization' : `Bearer ${tokenFromStorage}`
               })
        })
        console.table(response);
    }

    public static updateExisting = async (note: Note) => {
        let tmpDueDate = new Date(note.dueDate)
        let tokenFromStorage = localStorage.getItem("token");
        const response = await fetch('/api/v1/notes', {
            method: 'PUT',
            headers: new Headers({
                 'content-type': 'application/json',
                 'Authorization' : `Bearer ${tokenFromStorage}`
                }),
            body: `{\"id\": ${note.id}, 
                    \"title\": \"${note.title}\",
                    \"description\": \"${note.description}\",
                    \"dueDate\": \"${tmpDueDate.toISOString()}\",
                    \"importance\": ${note.importance},
                    \"done\": ${note.done}
                }`
        })
        console.table(response);
    }

    public static getNotes = async (completion: (notes: NotesResponse) => void, comparer: (a: Note, b: Note) => number) => {
        let tokenFromStorage = localStorage.getItem("token");
        console.log(tokenFromStorage);
        const response = await fetch('/api/v1/notes', {
          method: 'GET',
          headers: new Headers({ 
            'content-type': 'application/json',
            'Authorization' : `Bearer ${tokenFromStorage}`
           })
        })
        const notesResponse = await response.json();
        const notes = ParseNotes.toNotesResponse(JSON.stringify(notesResponse))
        notes.notes.sort(comparer)
        completion(notes);
      }

      public static login = async (withUsername: string, andPassword: string, onSuccess: () => void, onFail: () => void) => {
        const response = await fetch('/api/v1/auth/login', {
          method: 'POST',
          headers: new Headers({ 'content-type': 'application/json' }),
          body: `{\"username\": \"${withUsername}\", \"password\": \"${andPassword}\"}`
        })
        if (response.ok) {
            const token = await response.json();
            const parsedToken = ParseTokenStructure.toTokenStructure(JSON.stringify(token));
            console.table(parsedToken.accessToken.token);
            localStorage.setItem("token", parsedToken.accessToken.token);
            onSuccess();
        } else {
            onFail();
        }
      }

}