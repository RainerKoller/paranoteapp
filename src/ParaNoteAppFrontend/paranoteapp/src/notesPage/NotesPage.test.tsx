import React from 'react'
import ReactDOM from 'react-dom'

import NotesPage from './NotesPage';

test('adds numbers and checks result',  () => {expect (2+2).toBe(4)})


test('renders the correct content', () => {
    const root = document.createElement("div")
    ReactDOM.render(<NotesPage />, root)


    expect(root.querySelector("div")?.className).toBe("NotesPage")
})

