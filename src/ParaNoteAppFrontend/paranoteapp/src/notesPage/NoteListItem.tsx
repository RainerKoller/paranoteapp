import React, { Component } from "react";
import './NotesPage.css';
import '../../src/components/applayout/applayout.css' 
import '../components/UIComponents/buttons.css'
import '../components/UIComponents/SegmentedPicker/Segmentedpicker.css'
import { ParseNotes, NotesResponse, Note } from '../common/Note'
import BackendService from "../services/BackendService";
import ImportanceSelector from "../components/UIComponents/ImportanceSelector/ImportanceSelector";



  export interface NoteListItemProp {
    note: Note
    onEditClicked: () => void
  }
  
  export interface NoteListItemState {
    note: Note
  }
  
  export default class NoteListItem extends React.Component<NoteListItemProp, NoteListItemState> {
    constructor(props: NoteListItemProp) {
      super(props)
      this.state = {
        note: props.note,
      }
    }
  
    private onCheckboxChanged = async () => {
      let tempNote = this.state.note
      tempNote.done = !tempNote.done;
      tempNote.doneDate = new Date()
      this.setState({note : tempNote})
      await BackendService.updateExisting(tempNote);
      window.location.reload();
    }

    private onDeleteClicked = async () => {
        BackendService.deleteNote(this.state.note);
        window.location.reload();
    }
  
    render() {
      const note = { ...this.state.note }
      let buttonType = note.done ? 
      (<button onClick={this.onDeleteClicked} className="pna__noteDeleteButton">Delete</button>) : (<button onClick={this.props.onEditClicked} className="pna__noteEditButton">Edit</button>)
      return (
          <div className={note.done ? "pna__noteDone" : ""}>
              <div className="pna__notelistitem">
                <div className="pna__importanceSection">
                    <ImportanceSelector onChangedSelection={()=>{}} interactive={false} minImportance={0} maxImportance={5} initImportance={note.importance}></ImportanceSelector>
                </div>
                <div className="pna__note-duedate">{note.dueDate.toDateString()}</div>
                <div className="pna__note-title">{note.title}</div>
                <div className={"pna__note-state_container"}>
                    <input disabled={note.done} type="checkbox" checked={note.done} onChange={this.onCheckboxChanged} ></input>
                    <div className="pna__note-finished-text">{note.done ? `${note.doneDate?.toDateString()}` : "Finshed"}</div>
                </div>
                <div className="pna__note-content">{note.description}</div>
                {buttonType}
            </div>
          </div>
      );
    }
  }