import React, { Component } from "react";
import './NotesPage.css';
import '../../src/components/applayout/applayout.css' 
import '../components/UIComponents/buttons.css'
import '../components/UIComponents/SegmentedPicker/Segmentedpicker.css'
import { Redirect, useHistory } from 'react-router-dom';
import { ParseNotes, NotesResponse, Note } from '../common/Note'
import { EditNotePageMode, EditNotePageModeUtil } from '../noteEditPage/NoteEditPage'
import NoteListItem from './NoteListItem'

  export interface NoteListProps {
      notes?: NotesResponse
      filter: boolean
  }
  
  export interface NoteListState {
    toEditPage: boolean
  }
  
  export default class NoteList extends React.Component<NoteListProps, NoteListState>{
    constructor(props: NoteListProps) {
        super(props)
        this.state = {
          toEditPage: false
      }
    }
  
    public render() {
      if (this.state.toEditPage) {
        return(
          <Redirect to='/editNote' />
        ) 
      }
      const notes = []
      if (this.props.notes != null) {
        if (this.props.notes?.notes.length ?? 0 > 0) {
            for (let i = 0; i < this.props.notes.count; i += 1) {
                if (this.props.notes != null) {
                  if (this.props.filter) {
                    if (this.props.notes!.notes[i].done === false){
                      notes.push(<NoteListItem key={this.props.notes!.notes[i].id} onEditClicked={() => this.navigateToEditPageForExisting(this.props.notes!.notes[i])} note={this.props.notes.notes[i]}></NoteListItem>) 
                    }
                  }
                  else {
                    notes.push(<NoteListItem key={this.props.notes!.notes[i].id} onEditClicked={() => this.navigateToEditPageForExisting(this.props.notes!.notes[i])} note={this.props.notes.notes[i]}></NoteListItem>) }
                  }
              }
                return (
                    <div className="pna__noteslist">
                      {notes}
                    </div>
                );
            } else {
              return (
                  <div className="pna__allNotesDoneMessage">
                    🥳 Good Job! All things done! 🥳
                  </div>
                );
            }
        } else {
            return (null)
        }
    }
  
    private navigateToEditPageForExisting = (note: Note) => {
      localStorage.setItem('mode', EditNotePageModeUtil.fromModeToString(EditNotePageMode.Editing))
      localStorage.setItem('note', JSON.stringify(note))
      this.setState({ toEditPage: true })
    }
  }
  