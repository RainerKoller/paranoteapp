import React from 'react'
import ReactDOM from 'react-dom'


import { NotesResponse, Note } from '../common/Note'
import NoteList from './NoteList'




test('renders the correct amount of notes', () => {
    const root = document.createElement("div")

    let notes : Note[] = []
    
     notes.push(
         {
        "id": 1,
        "title": "MyTitle1",
        "description": "MyDescription1",
        "createdDate": new Date(),
        "done": false,
        "dueDate": new Date(),
        "importance": 3
     })

     notes.push(
        {
       "id": 2,
       "title": "MyTitle2",
       "description": "MyDescription2",
       "createdDate": new Date(),
       "done": false,
       "dueDate": new Date(),
       "importance": 4
    })


    var nr : NotesResponse = {
        "notes": notes,
        "count": 2,
        "success": true
    }

    ReactDOM.render(<NoteList filter={false} notes={nr} />, root)

    expect(root.querySelector("div")?.childElementCount).toBe(2)
})


test('renders the correct amount of notes', () => {
    const root = document.createElement("div")

    let notes : Note[] = []
    
     notes.push(
         {
        "id": 1,
        "title": "MyTitle1",
        "description": "MyDescription1",
        "createdDate": new Date(),
        "done": false,
        "dueDate": new Date(),
        "importance": 3
     })

     notes.push(
        {
       "id": 2,
       "title": "MyTitle2",
       "description": "MyDescription2",
       "createdDate": new Date(),
       "done": false,
       "dueDate": new Date(),
       "importance": 4
    })


    var nr : NotesResponse = {
        "notes": notes,
        "count": 2,
        "success": true
    }

    ReactDOM.render(<NoteList filter={false} notes={nr} />, root)

    var n = root.querySelector("div")?.firstElementChild?.getElementsByClassName("pna__note-content")[0]
    .textContent


    expect(n).toBe("MyDescription1")
})
