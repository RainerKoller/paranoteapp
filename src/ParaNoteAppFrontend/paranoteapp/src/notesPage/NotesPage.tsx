import React, { Component } from "react";
import './NotesPage.css';
import '../../src/components/applayout/applayout.css' 
import '../components/UIComponents/buttons.css'
import '../components/UIComponents/SegmentedPicker/Segmentedpicker.css'
import SegmentedPicker, { SegmentedPickerOption} from '../components/UIComponents/SegmentedPicker/SegmentedPicker'
import { Redirect, useHistory } from 'react-router-dom';
import { ParseNotes, NotesResponse, Note } from '../common/Note'
import { EditNotePageMode, EditNotePageModeUtil } from '../noteEditPage/NoteEditPage'
import Toggle from '../components/UIComponents/Toggle/Toggle'
import NoteList from './NoteList'
import BackendService from "../services/BackendService";

interface NotesPageState {
  noteresponse?: NotesResponse;
  toLoginPage: boolean;
  toEditPage: boolean;
  darkModeOn: boolean;
  showOpenOnly: boolean;
  segmentedPickerSetting: SegmentedPickerOption;
}

interface NotesPageProps {

}


export default class NotesPage extends React.Component<NotesPageProps, NotesPageState> {

  constructor(props: NotesPageProps) {
    super(props)
    this.state = {
      toLoginPage: false,
      toEditPage: false,
      darkModeOn: this.getDarkModePreference(),
      showOpenOnly: this.getShowOpenOnlyPreference(),
      segmentedPickerSetting : this.getSegmentedPickerSelection()
    }
  }

  componentDidMount() {
    BackendService.getNotes(notesResponse => {
      this.setState({ noteresponse: notesResponse })}, 
      this.compareDone);
  }

  private compareDone(a: Note, b: Note){
    if ((a.doneDate ?? new Date()) < (b.doneDate ?? new Date())){
      return -1
    }
    else if ((a.doneDate ?? new Date()) > (b.doneDate ?? new Date())){
      return 1
    }
    else {
      return 0
    }
  }

  private compareImportance(a: Note, b: Note) {
    if (a.importance > b.importance){
      return -1
    }
    else if (a.importance < b.importance){
      return 1
    }
    else {
      return 0
    }
  }

  private compareCreated(a: Note, b: Note) {
    if (a.createdDate < b.createdDate){
      return -1
    }
    else if (a.createdDate > b.createdDate){
      return 1
    }
    else {
      return 0
    }
  }  

  private storeSegmentedPickerSelection(opt: SegmentedPickerOption) : void{
    console.log("StoreSegmentedPickerSelection")
    localStorage.setItem('segmentedPickerOption', opt)
  }

  private getSegmentedPickerSelection(): SegmentedPickerOption {
    const pickeroption = localStorage.getItem('segmentedPickerOption')
    let ret = SegmentedPickerOption.ByImportance
    if (pickeroption != null){
      ret = this.toSegmentedPickerOption(pickeroption)
    }
    return ret
  }

  private toSegmentedPickerOption(x: any): SegmentedPickerOption {
    switch (String(x)){
      case "Done":
        return SegmentedPickerOption.ByFinishDate
      case "Created":
        return SegmentedPickerOption.ByCreatedDate
      case "Importance":
        return SegmentedPickerOption.ByImportance
      default:
        return SegmentedPickerOption.ByImportance
    }
  }
  
  private getDarkModePreference(): boolean {
    const darkModeOn = localStorage.getItem('darkMode')
    return darkModeOn == "on"
  }

  private getShowOpenOnlyPreference(): boolean {
    const showOpenOnly = localStorage.getItem('showOpenOnly')
    return showOpenOnly == "on"
  }

  private onDarkModeToggled = async (isToggled: boolean) => {
    console.log("Dark Mode is: " + (isToggled ? "on" : "off"))
    localStorage.setItem('darkMode', isToggled ? "on" : "off")
    await this.setState( {darkModeOn: isToggled} )
  }

  private onFilterOpenOnlyToggled = async (isToggled: boolean) => {
    console.log("Filter Done only is: " + (isToggled ? "on" : "off"))
    localStorage.setItem('showOpenOnly', isToggled ? "on" : "off")
    await this.setState( {showOpenOnly: isToggled} )
  }

  private isAnyNoteDone = (notesResponse?: NotesResponse) : boolean => {
    if (notesResponse != null) {
      for (const note of notesResponse.notes) {
        if (note.done) {
          return true;
        }
      }
    }
    return false;
  }

  public render() {
    if (this.state.toEditPage) {
      return(
          <Redirect to='/editNote' />
      )
    }
    return(
      <div className="NotesPage">
        <div className={this.state.darkModeOn ? 'darkTheme' : ''}> 
          <div className="background-image"/>
          <div className="pna__container">
            <div className="pna__topbuttonsbar">
              <button onClick={this.navigateToEditPageForNew} className="button">Create new Note</button>
              <Toggle onChangedTo={this.onDarkModeToggled} state0Text="Dark Mode" state1Text="Light Mode" isToggled={this.state.darkModeOn}></Toggle>
            </div>
            <div className="pna__filtersection">
              <SegmentedPicker hideDone={this.state.showOpenOnly || !this.isAnyNoteDone(this.state.noteresponse)} defaultSelection={this.state.segmentedPickerSetting} onChangedSelection={this.onPickerChanged}></SegmentedPicker>
              <Toggle onChangedTo={this.onFilterOpenOnlyToggled} isToggled={this.state.showOpenOnly} state0Text="Open" state1Text="All"></Toggle>
            </div>
            <NoteList filter={this.state.showOpenOnly} notes={this.state.noteresponse}></NoteList>
          </div>
        </div>
      </div>
    )
  }

  private onPickerChanged = async (option: SegmentedPickerOption) => {
    switch(option) {
      case SegmentedPickerOption.ByFinishDate:
        console.log("Done")
        this.storeSegmentedPickerSelection(SegmentedPickerOption.ByFinishDate)
        let mynoteresponsedone = this.state.noteresponse
        mynoteresponsedone?.notes.sort(this.compareDone)
        await this.setState({ noteresponse: mynoteresponsedone})
        break
      case SegmentedPickerOption.ByCreatedDate:
        console.log("Created")
        this.storeSegmentedPickerSelection(SegmentedPickerOption.ByCreatedDate)
        let mynoteresponsecreated = this.state.noteresponse
        mynoteresponsecreated?.notes.sort(this.compareCreated)
        await this.setState({ noteresponse: mynoteresponsecreated})
        break
      case SegmentedPickerOption.ByImportance:
        console.log("Importance")
        this.storeSegmentedPickerSelection(SegmentedPickerOption.ByImportance)
        let mynoteresponseimportance = this.state.noteresponse
        mynoteresponseimportance?.notes.sort(this.compareImportance)
        await this.setState({ noteresponse: mynoteresponseimportance})
        break
      }
    }

  private navigateToEditPageForNew = () => {
    localStorage.setItem('mode', EditNotePageModeUtil.fromModeToString(EditNotePageMode.CreateNew))
    localStorage.removeItem('note')
    this.setState({ toEditPage: true })
  }
}
