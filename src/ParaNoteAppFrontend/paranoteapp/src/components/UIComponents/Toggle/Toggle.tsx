import React from "react";
import "./Toggle.css";

  export interface IToggleButtonProps {
    isToggled: boolean;
    state0Text: string;
    state1Text: string;
    onChangedTo: (toggled: boolean) => void;
  }
  
  export interface IToggleButtonState {
    toggled: boolean;
  }

  export default class Toggle extends React.Component<IToggleButtonProps, IToggleButtonState> {
    constructor(props: IToggleButtonProps) {
      super(props);
      this.state = { toggled: props.isToggled };
    }

    private onClicked = async () => {
        await this.setState({toggled: !this.state.toggled})
        this.props.onChangedTo(this.state.toggled)
    }
  
    render() {
      return (
        <div className={this.state.toggled ? "pna__isToggled" : ""}>
          <div className="pna__custom-select" onClick={this.onClicked}> { this.state.toggled ? this.props.state0Text : this.props.state1Text}</div>
        </div>
      );
    }
  }