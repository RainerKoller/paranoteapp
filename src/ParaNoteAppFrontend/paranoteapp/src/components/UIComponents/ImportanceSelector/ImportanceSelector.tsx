import React, { Component, ReactHTML } from "react";
import '../ImportanceSelector/ImportanceSelector.css'

export interface ImportanceSelectorProps {
    interactive: boolean
    minImportance: number
    maxImportance: number
    initImportance?: number
    onChangedSelection: (importance: number) => void;
}
  
export interface ImportanceSelectorState {
    selectedId?: number;
}

export default class ImportanceSelector extends React.Component<ImportanceSelectorProps, ImportanceSelectorState> {
    constructor(props: ImportanceSelectorProps) {
        super(props);
        this.state = { selectedId: props.initImportance };
    }

    private onImportanceChanged = (id: number) => {
        this.props.onChangedSelection(id)
        this.setState({ selectedId: id})
    }

    async componentDidMount() {
        await this.setState( {selectedId: this.props.initImportance} )
    }

    render() {
        const singleImportances = []
        for (let i = this.props.minImportance; i < ( this.props.interactive ? this.props.maxImportance : this.props.initImportance ?? 0); i += 1) { 
            singleImportances.push(<SingleImportance interactive={this.props.interactive} highlighted={ i < (this.state.selectedId ?? this.props.initImportance ?? 0)} onClicked={this.onImportanceChanged} id={i}></SingleImportance>)
        }
        return (
            <div className={this.props.interactive ? "pna__interactive" : ""}>
                <div className="pna__importancepickercontainer"> 
                    {this.props.interactive ? <div className="pna__importancetitle">Importance: </div> : null}
                    <div className="pna__importances">
                        {singleImportances}
                    </div>
                </div>
            </div>
        );
    }

}

interface SingleImportanceProps {
    id: number
    highlighted: boolean;
    interactive: boolean;
    onClicked: (id: number) => void;
}

function SingleImportance(props: SingleImportanceProps) {
    return (
        <div onClick={() => props.onClicked(props.id+1)} className="pna__singleImportanceContainer">
            <div className={props.highlighted ? "pna__highlighted" : ""}>
                <div className="pna__singleimportance">!</div>
            </div>
        </div>
    );
}
