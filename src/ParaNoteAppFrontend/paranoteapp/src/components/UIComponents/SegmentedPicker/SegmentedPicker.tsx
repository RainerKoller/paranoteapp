import React from "react";

export enum SegmentedPickerOption {
    ByFinishDate = "Done",
    ByCreatedDate = "Created",
    ByImportance = "Importance"
}

export interface SegmentedPickerProps {
    hideDone: boolean
    defaultSelection: SegmentedPickerOption
    onChangedSelection: (option: SegmentedPickerOption) => void;
}

  
export interface SegmentedPickerState {
    selectedOption: SegmentedPickerOption
}


export default class SegmentedPicker extends React.Component<SegmentedPickerProps, SegmentedPickerState> {
    constructor(props: SegmentedPickerProps) {
        super(props);
        this.state = {
            selectedOption: this.props.defaultSelection
        }
    }

    private onFilterSelectionChanged = (opt: SegmentedPickerOption) => {
        this.props.onChangedSelection(opt)
        this.setState( {selectedOption : opt} )
    }

    render(){
        let options = [ 
            SegmentedPickerOption.ByFinishDate, 
            SegmentedPickerOption.ByCreatedDate, 
            SegmentedPickerOption.ByImportance 
        ]
        if (this.props.hideDone) {
            options = [ 
                SegmentedPickerOption.ByCreatedDate, 
                SegmentedPickerOption.ByImportance 
            ]
        }
    return (
            <div className="pna__segmentedpicker"> 
                {options.map(opts => 
                    <div className={opts == this.state.selectedOption ? "pna__selectedPicker" : ""}>
                        <div className="pna__segmentedpickeritem" onClick={() => this.onFilterSelectionChanged(opts)}>{opts}</div>
                    </div>
                )}
            </div>
        
        )}
}

