import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import NotesPage from './notesPage/NotesPage';
import LoginPage from './loginPage/LoginPage';
import * as serviceWorker from './serviceWorker';
import NoteEditPage from './noteEditPage/NoteEditPage';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Route exact path="/" component={LoginPage} />
      <Route path="/notes" component={NotesPage} />
      <Route path="/editNote" component={NoteEditPage} />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
