
# Overview

This document provides a detailed description of the *ParaNoteApp*.

## Project Team

The project team consists of following developers:

- Pascal Burlet (pascal.burlet@ost.ch) 
- Rainer Koller (rainer.koller@ost.ch) 


# System

The *ParaNoteApp* consists of a frontend written in typescript / react, and of a backend written in .net core 3.1


# Get the project

Navigate to your projects folder `<PROJ_DIR>` (a directory of your choice) and clone the git repository there. 

```shell
$ git clone https://gitlab.com/RainerKoller/paranoteapp.git
```

The project root for the frontend can be found in `<FE_DIR>` = `<PROJ_DIR>`/src/ParaNoteAppFrontend/paranoteapp

The project root for the backend can be found in `<BE_DIR>` = `<PROJ_DIR>`/src/ParaNoteAppBackend



# Features

## Frontend


The application provides following features:

- It is written in typescript
- Focus on FED (Animations and UI)
- User login
    - Failure message when entering wrong credentials
- Notes overview page
    - List of notes that belong to the logged in user
    - Sort the list by ***Done***, ***Importance*** and ***Creation date*** *(persisted)*
    - Toggle the themes (bright / dark) *(persisted)*
    - Mark a note as finished (done)
    - Filter all notes that are marked as done *(persisted)*
    - Select if a new note should be added
    - Select if an existing note should be edited
    - Select if an existing note (which is in state: *done*) should be deleted
- Note editing page
    - Custom made ***importance picker component***
    - Change or create a note
    - Cancel the process
    - Validating the input (is title field not empty)
    - Select the importance via 'Importance Control'
    - Deleting of a note
- Unit tests
    - A very basic set of unit tests are implemented (`<FE_DIR>/src/notesPage/NoteList.test.tsx`)



## Backend


The application provides following features:

- It is written in .netcore 3.1
- Message validation (i.e. `<BE_DIR>`/Web.Api.Core/Validation/AddNoteValidator.cs)
- Data persistence in a sqlite database (`<PROJ_DIR>`/sql/local.db)
- Unit Tests (i.e. `<BE_DIR>`/Tests/Web.Api.Core.UnitTests/UseCases/LoginUseCaseUnitTests.cs)
- Integration Tests (i.e. `<BE_DIR>`/Tests/Web.Api.IntegrationTests/Controllers/NotesControllerIntegrationTests.cs)
- Authentication (i.e. `<BE_DIR>`/Web.Api/Controllers/AuthController.cs)
- Authorization (i.e. `<BE_DIR>`/Web.Api/Controllers/NotesController.cs : line 56)
- Database migration with code first (entity framework)
- Swagger Api documentation (http://localhost:5000/swagger) when backend server is running




# Run the application

## Backend

This section describes the installation and build process. It can be ran on either windows, linux or mac


Navigate to your projects-root directory (`<BE_DIR>`) and type:


```shell
$ dotnet publish -c Release -r <RID>
```

Example values for `<RID>` (runtime identifier)

 **Name**  | **OS**
-----------|--------------------
 win10-x64 | windows 10 (64 bit)
 osx-x64   | mac os (64 bit)
 linux-x64 | linux (64 bit)


------------------------------------------------------------
![note](doc/img/info.png)\  for any further `<RID>`'s please see: https://docs.microsoft.com/en-us/dotnet/core/rid-catalog

------------------------------------------------------------



### Start the application

For debug purposes, we can start the application as follows:

```shell
$ cd Web.Api/bin/Release/netcoreapp3.1/<RID>/publish
$ dotnet Web.Api.dll
```

------------------------------------------------------------
![note](doc/img/note.png)\  Note that the staring of the application has to be done from the directory it resides.

------------------------------------------------------------
------------------------------------------------------------
![note](doc/img/info.png)\  if you start the backend server with the command 'dotnet Web.Api.dll --Production', the application will check in the appsettings.Production.json for the database connection string 

------------------------------------------------------------



## Frontend

------------------------------------------------------------
![important](doc/img/important.png)\  Before starting the frontend, the backend server should be running. 

------------------------------------------------------------



Navigate to `<FE_DIR>` and type:


```shell
$ npm install
$ npm start
```


## Preconfigured users

Following users are preconfigured and stored in the database (`<PROJ_DIR>`/sql/local.db)

 **User** | **Password**
 ---------|-------------
 rainer   | rainer
 pascal   | pascal
 tobi     | tobitobi


### Small hint

Please login with all of the above mentioned users and check their notes.
